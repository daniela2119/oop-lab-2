class Student():
    def __init__(self, surname, name, fathersName, birthday, adress, phoneNumber, major, year):
        self.surname = surname
        self.name = name
        self.fathersName = fathersName
        self.birthday = birthday
        self.adress = adress
        self.phoneNumber = phoneNumber
        self.major = major
        self.year = year

student1 = Student("Анастасьева", "Анастасия", "Игоревна", "12.03.1999", "Киев", "0501234567", "КИ", "2")
student2 = Student("Горбатюк", "Сергей", "Михайлович", "23.04.1999", "Днепр", "0508901234", "КИ", "2")
student3 = Student("Полякова", "Валерия", "Витальевна", "30.05.2000", "Белая Цевковь", "0505678901", "АИ", "2")
student4 = Student("Гребеньков", "Антон", "Олегович", "04.06.2000", "Киев", "0502345678", "КН", "1")
student5 = Student("Лобанёв", "Денис", "Алексеевич", "05.07.1999", "Ивано-франковск", "0509012345", "ТЕ", "3")

group = [student1, student2, student3, student4, student5]

#код ниже выводит список студентов заданого факультета

major = input("Введите название группы: ")

for st in group:
    if major == st.major:
        print(st.surname + " " + st.name + " " + st.fathersName)

#код ниже выводит список студентов дла каждого факультета и курса

for st in group:
    print(st.surname + " " + st.name + " " + st.fathersName + " учится на " + st.major + "-" + st.year)

#код ниже выводит список студентов, которые родились после заданого года

birthYear = input("Введите год рождения: ")

for st in group:
    year = st.birthday[6:]
    if int(birthYear) <= int(year):
        print(st.surname + " " + st.name + " " + st.fathersName)
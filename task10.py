class Customer():
    def __init__(self, surname, name, fathersName, adress, phoneNumber, creditCartNumber, bankAccountNumber):
        self.surname = surname
        self.name = name
        self.fathersName = fathersName
        self.adress = adress
        self.phoneNumber = phoneNumber
        self.creditCartNumber = creditCartNumber
        self.bankAccountNumber = bankAccountNumber

customer1 = Customer("Анастасьева", "Анастасия", "Игоревна", "Киев", "0501234567", 191007, 100001)
customer2 = Customer("Горбатюк", "Сергей", "Михайлович", "Днепр", "0508901234", 110807, 650023)
customer3 = Customer("Полякова", "Валерия", "Витальевна", "Белая Цевковь", "0505678901", 240512, 210004)
customer4 = Customer("Гребеньков", "Антон", "Олегович", "Киев", "0502345678", 100906, 880056)
customer5 = Customer("Лобанёв", "Денис", "Алексеевич", "Ивано-франковск", "0509012345", 110812, 440007)

group = [customer1, customer2, customer3, customer4, customer5]

#код ниже выводит список покупателей, фамилия которых начинается с заданной буквы

letter = input("Введите букву: ")

for cu in group:
    surnameLetter = cu.surname[0]
    if letter == surnameLetter:
        print(cu.surname, cu.name, cu.fathersName)

#код ниже выводит список покупателей, номер кредитной карты которых находится в заданном интревале

minNum = input("Введите минимальный номер карты: ")
maxNum = input("Введите максимальный номер карты: ")

for cu in group:
    if int(minNum) <= cu.creditCartNumber and int(maxNum) >= cu.creditCartNumber:
        print(cu.surname, cu.name, cu.fathersName)

#код ниже выводит сведенья про покупателя, по номеру определённого банковского счёта

bankAccountNumber = input("Введите банковский счёт: ")

for cu in group:
    if int(bankAccountNumber) == cu.bankAccountNumber:
        print(cu.surname, cu.name, cu.fathersName, "-", cu.phoneNumber)
class File():
    def __init__(self, name, size, date, callsNumber):
        self.name = name
        self.size = size
        self.date = date
        self.callsNumber = callsNumber

file1 = File("task1", 1000, "12.03.1999", 191007)
file2 = File("task2", 6500, "23.04.1999", 110807)
file3 = File("task4", 2100, "30.05.2000", 240512)
file4 = File("task8", 8800, "04.06.2000", 100906)
file5 = File("oop16", 4400, "05.07.1999", 110812)

group = [file1, file2, file3, file4, file5]

#код ниже выводит список файлов, дата создания которых принадлежит заданному диапазону

minDate = input("Введите минимальную дату: ")
dMinDay = minDate[0:2]
dMinMonth = minDate[3:5]
dMinYear = minDate[6:]
maxDate = input("Введите максимальную дату: ")
dMaxDay = maxDate[0:2]
dMaxMonth = maxDate[3:5]
dMaxYear = maxDate[6:]

for fi in group:
    day = fi.date[0:2]
    month = fi.date[3:5]
    year = fi.date[6:]
    if int(dMinYear) < int(year) and int(dMaxYear) > int(year):
        print(fi.name)
    elif int(dMinYear) == int(year):
        if int(dMinMonth) < int(month):
            print(fi.name)
        elif int(dMinMonth) == int(month):
            if int(dMinDay) <= int(month):
                print(fi.name)
    elif int(dMaxYear) == int(year):
        if int(dMaxMonth) > int(month):
            print(fi.name)
        elif int(dMaxMonth) == int(month):
            if int(dMaxDay) >= int(month):
                print(fi.name)

#код ниже выводит список файлов, размер которых превышает заданный

size = input("Введите размер файла: ")

for fi in group:
    if int(size) <= fi.size:
        print(fi.name)

#код ниже выводит список файлов, название которых начинется с заданной буквы

letter = input("Введите букву: ")

for fi in group:
    nameLetter = fi.name[0]
    if letter == nameLetter:
        print(fi.name)
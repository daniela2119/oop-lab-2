class CountryCitizen():
    def __init__(self, surname, name, fathersName, adress, gender, major, birhtday):
        self.surname = surname
        self.name = name
        self.fathersName = fathersName
        self.adress = adress
        self.gender = gender
        self.major = major
        self.birhtday = birhtday

citizen1 = CountryCitizen("Анастасьева", "Анастасия", "Игоревна", "Киев", "female", "КИ", "12.03.1999")
citizen2 = CountryCitizen("Горбатюк", "Сергей", "Михайлович", "Днепр", "male", "КИ", "23.04.1999")
citizen3 = CountryCitizen("Полякова", "Валерия", "Витальевна", "Белая Цевковь", "female", "АИ", "30.05.2000")
citizen4 = CountryCitizen("Гребеньков", "Антон", "Олегович", "Киев", "male", "КН", "04.06.2000")
citizen5 = CountryCitizen("Лобанёв", "Денис", "Алексеевич", "Ивано-франковск", "male", False, "05.07.1999")

group = [citizen1, citizen2, citizen3, citizen4, citizen5]

#код ниже выводит список граждан, возраст которых превышает заданный

age = input("Введите возраст: ")

for ci in group:
    ciAge = ci.birhtday[6:]
    if int(age) == 2020 - int(ciAge):
        print(ci.surname + " " + ci.name + " " + ci.fathersName)

#код ниже выводит список граждан с высшем образованием

for ci in group:
    if ci.major:
        print(ci.surname + " " + ci.name + " " + ci.fathersName)

#код ниже выводит список граждан мужского пола

for ci in group:
    if ci.gender == "male":
        print(ci.surname + " " + ci.name + " " + ci.fathersName)
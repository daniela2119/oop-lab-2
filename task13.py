class ScienceArticle():
    def __init__(self, title, author, rubric, magazineName, date):
        self.title = title
        self.author = author
        self.rubric = rubric
        self.magazineName = magazineName
        self.date = date

article1 = ScienceArticle("Замок из стекла", "Джаннетт Уоллс", "Инженерия", "Бомбора", "12.03.1999")
article2 = ScienceArticle("Дивергент", "Вероника Рот","Социология",  "Эксмо", "23.04.1999")
article3 = ScienceArticle("Убить пресмешника", "Харпер Ли", "Птицы", "АСТ", "30.05.2000")
article4 = ScienceArticle("Инсургент", "Вероника Рот", "Социология", "Эксмо", "04.06.2000")
article5 = ScienceArticle("Мятная сказка", "Александр Полярный", "Мифы", "АСТ", "05.07.1999")

group = [article1, article2, article3, article4, article5]

#код ниже выводит список статей заданного журнала

magazine = input("Введите название журнала: ")

for ar in group:
    if magazine == ar.magazineName:
        print(ar.title)

#код ниже выводит список авторов, которые имеют больше чем N изданных статей

n = int(input("Введите количество статей: "))

for ar in group:
    i = 0
    for art in group:
        if ar.author == art.author:
            i = i + 1
    if i >= n:
        print(ar.title)

#код ниже выводит сведенья про публикацию с самой поздней датой выхода

for ar in group:
    day1 = int(ar.date[0:2])
    month1 = int(ar.date[3:5])
    year1 = int(ar.date[6:])
    if ar.rubric == article5.rubric:
        print(i.title)
    for art in group:
        day2 = int(art.date[0:2])
        month2 = int(art.date[3:5])
        year2 = int(art.date[6:])
        if year1 > year2:
            i = ar
        elif year1 == year2:
            if month1 > month2:
                i = ar
            elif month1 == month2:
                if day1 > day2:
                    i = ar
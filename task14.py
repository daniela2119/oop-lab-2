class Apartment():
    def __init__(self, adress, floor, space, ammountOfRooms, price):
        self.adress = adress
        self.floor = floor
        self.space = space
        self.ammountOfRooms = ammountOfRooms
        self.price = price

flat1 = Apartment("Киев", 36, 33, 2, 1000)
flat2 = Apartment("Днепр", 1, 340, 3, 6500)
flat3 = Apartment("Белая Цевковь", 3, 50, 2, 2100)
flat4 = Apartment("Киев", 10, 34, 1, 8800)
flat5 = Apartment("Ивано-франковск", 1, 70, 2, 4400)

group = [flat1, flat2, flat3, flat4, flat5]

#код ниже выводит список квартир с указанным количеством комнат

rooms = input("Введите количество комнат: ")

for fl in group:
    if int(rooms) == fl.ammountOfRooms:
        print("Квартира в городе", fl.adress, "на", fl.floor, "этаже")

#код ниже выводит сведенья про квартиры, цена на которые не превышает заданную

price = input("Введите цену: ")

for fl in group:
    if int(price) >= fl.price:
        print("Квартира в городе", fl.adress, "на", fl.floor, "этаже")

#код ниже выводит список квартир с отличием в площади в пределах 10% от заданной

space = int(input("Введите площадь: "))
difference = (space / 100) * 10

for fl in group:
    if space - difference <= fl.space and space + difference >= fl.space:
        print("Квартира в городе", fl.adress, "на", fl.floor, "этаже")
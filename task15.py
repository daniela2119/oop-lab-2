class ApartmentOnStock():
    def __init__(self, adress, floor, space, ammountOfRooms, price):
        self.adress = adress
        self.floor = floor
        self.space = space
        self.ammountOfRooms = ammountOfRooms
        self.price = price

flat1 = ApartmentOnStock("Киев", 36, 33, 2, 1000)
flat2 = ApartmentOnStock("Днепр", 1, 340, 3, 6500)
flat3 = ApartmentOnStock("Белая Цевковь", 3, 50, 2, 2100)
flat4 = ApartmentOnStock("Киев", 10, 34, 1, 8800)
flat5 = ApartmentOnStock("Ивано-франковск", 1, 70, 2, 4400)

group = [flat1, flat2, flat3, flat4, flat5]

#код ниже выводит список квартир, которые имеют заданное число число комнат

rooms = input("Введите количество комнат: ")

for fl in group:
    if int(rooms) == fl.ammountOfRooms:
        print("Квартира в городе", fl.adress, "на", fl.floor, "этаже")

#код ниже выводит список квартир, которые расположены на этаже, который находится на заданном промежутке

maxFloor = input("Введите максимальный этаж: ")
minFloor = input("Введите минимальный этаж: ")

for fl in group:
    if int(maxFloor) >= fl.floor and int(minFloor) <= fl.floor:
        print("Квартира в городе", fl.adress, "на", fl.floor, "этаже")

#код ниже выводит список квартир, которые имеют площадь, которая не превышает заданную

space = int(input("Введите площадь: "))

for fl in group:
    if space <= fl.space:
        print("Квартира в городе", fl.adress, "на", fl.floor, "этаже")
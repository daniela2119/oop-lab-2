class Computer():
    def __init__(self, CPU, motherboard, hardDiscSpace, RAMSize, graphicsCard, isDVDDrive):
        self.CPU = CPU
        self.motherboard = motherboard
        self.hardDiscSpace = hardDiscSpace
        self.RAMSize = RAMSize
        self.graphicsCard = graphicsCard
        self.isDVDDrive = isDVDDrive

computer1 = Computer("Core i3", "Gigabyte GA-H110", 512, 4, "Gigabyte PCI-Ex", False)
computer2 = Computer("Core i5", "Asus M5A78L", 128, 8, "MSI PCI-Ex", False)
computer3 = Computer("Core i7", "MSI B450", 256, 1, "Asus PCI-Ex", True)
computer4 = Computer("Core i9", "ASRock Fatal1", 1024, 2, "Gigabyte PCI-Ex", True)
computer5 = Computer("Ryzen 7", "Gigabyte GA-H110", 128, 0.5, "MSI PCI-Ex", True)

group = [computer1, computer2, computer3, computer4, computer5]

#код ниже выводит список компьютеров с объёмом жёсткого диска больше 600Гб и ОЗП больше 1026Мб(1Гб)

for co in group:
    if co.hardDiscSpace >= 600 and co.RAMSize >= 1:
        print("Компьютер с процессором", co.CPU, "и оперативной памятью", co.RAMSize)

#код ниже выводит список компьютеров в компиляции котороых нет DVD привода

for co in group:
    if !co.isDVDDrive:
        print("Компьютер с процессором", co.CPU, "и оперативной памятью", co.RAMSize)

#код ниже выводит список компьютеров, которые имеют указанный тип процессора

CPU = input("Введите тип процессора: ")

for co in group:
    if CPU == co.CPU:
        print("Компьютер с процессором", co.CPU, "и оперативной памятью", co.RAMSize)
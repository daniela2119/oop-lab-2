class AutoService():
    def __init__(self, registrationNumber, model, mileage, repairMaster, price):
        self.registrationNumber = registrationNumber
        self.model = model
        self.mileage = mileage
        self.repairMaster = repairMaster
        self.price = price

auto1 = AutoService(191007, "Honda", 10000, "Анастасьева А. И.", 3302)
auto2 = AutoService(110807, "Tesla", 6500, "Горбатюк С. М.", 3400)
auto3 = AutoService(240512, "Hundai", 21000, "Полякова В. В.", 3506)
auto4 = AutoService(191007, "Honda", 88000, "Гребеньков А. О.", 3412)
auto5 = AutoService(110812, "BMW", 44000, "Анастасьева А. И.", 7020)

group = [auto1, auto2, auto3, auto4, auto5]

#код ниже выводит ведомости про ремонт по регистрационному номеру автомобиля

registrationNumber = input("Введите номер автомобиля: ")

for au in group:
    if int(registrationNumber) == au.registrationNumber:
        print("Машину", au.model, "ремонтировал(а)", au.repairMaster + " Стоимость ремонта:", au.price)

#код ниже выводит список выполненных ремонтов заданным мастером

repairMaster = input("Введите имя мастера: ")

for au in group:
    if repairMaster == au.repairMaster:
        print("Машину", au.model, "ремонтировал(а)", au.repairMaster + " Стоимость ремонта:", au.price)

#код ниже выводит список регистрационных номеров автомобилей с маркой авто, которая чаще всего находится на автосервисе

j = 0

for au in group:
    i = 0
    for aut in group:
        if au.registrationNumber == aut.registrationNumber:
            i = i + 1
    if  j < i:
        j = i
        n = au
    if au.registrationNumber == auto5.registrationNumber:
        print(n.model, n.registrationNumber)
class Subscriber():
    def __init__(self, surname, name, fathersName, adress, phoneNumber, conversationTime, tariff):
        self.surname = surname
        self.name = name
        self.fathersName = fathersName
        self.adress = adress
        self.phoneNumber = phoneNumber
        self.conversationTime = conversationTime
        self.tariff = tariff

subscriber1 = Subscriber("Анастасьева", "Анастасия", "Игоревна", "Киев", "0501234567", 36, "SuperNet Start")
subscriber2 = Subscriber("Горбатюк", "Сергей", "Михайлович", "Днепр", "0508901234", 1, "SuperNet Pro")
subscriber3 = Subscriber("Полякова", "Валерия", "Витальевна", "Белая Цевковь", "0505678901", 3, "SuperNet Unlim")
subscriber4 = Subscriber("Гребеньков", "Антон", "Олегович", "Киев", "0502345678", 1, "Family Plus")
subscriber5 = Subscriber("Лобанёв", "Денис", "Алексеевич", "Ивано-франковск", "0509012345", 61, "Devise S")

group = [subscriber1, subscriber2, subscriber3, subscriber4, subscriber5]

#код ниже выводит список абонентов, время разговора которых превышает заданное

conversationTime = input("Введите время разговора: ")

for su in group:
    if int(conversationTime) <= su.conversationTime:
        print(su.surname + " " + su.name + " " + su.fathersName)

#код ниже выводит сведенья про абонента, по введённому номеру телефона

phoneNumber = input("Введите номер телефона: ")

for su in group:
    if phoneNumber == su.phoneNumber:
        print(su.surname + " " + su.name + " " + su.fathersName, "использует тариф", su.tariff)

#код ниже выводит ведомости про обонентов, сумма оплаты разговоров которых превышает заданную

for su in group:
    if su.conversationTime >= 60:
        print(su.surname + " " + su.name + " " + su.fathersName)
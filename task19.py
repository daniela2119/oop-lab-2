class Minibus():
    def __init__(self, wayNumber, departureTime, arrivalTime, wayTime, busStation, isDoingCircle):
        self.wayNumber = wayNumber
        self.departureTime = departureTime
        self.arrivalTime = arrivalTime
        self.wayTime = wayTime
        self.busStation = busStation
        self.isDoingCircle = isDoingCircle

bus1 = Minibus(191007, "12:34", "13:56", "01:22", "Ocean Plaza", False)
bus2 = Minibus(110807, "05:06", "07:06", "02:00", "ст м Дружбы Народов", True)
bus3 = Minibus(240512, "08:07", "08:34", "00:27", "Ocean Plaza", True)
bus4 = Minibus(100906, "09:01", "10:56", "01:55", "ст м Дружбы Народов", False)
bus5 = Minibus(110812, "23:45", "23:59", "00:14", "Крещатик", True)

group = [bus1, bus2, bus3, bus4, bus5]

#код ниже выводит информацию про маршрутное такси заданного номера

wayNumber = input("Введите номер маршрутного такси: ")

for bu in group:
    if int(wayNumber) == bu.wayNumber:
        print("Маршрутное такси №", bu.wayNumber, "отправляется в " + bu.departureTime)

#код ниже выводит список номеров маршрутных такси, которые начинают движения и заданного пункта

station = input("Введите название остановки: ")

for bu in group:
    if station == bu.busStation:
        print("Маршрутное такси №", bu.wayNumber, "отправляется в " + bu.departureTime)

#код ниже выводит список номеров маршрутных такси, которые имеют кольцевой маршрут

for bu in group:
    if bu.isDoingCircle:
        print("Маршрутное такси №", bu.wayNumber, "отправляется в " + bu.departureTime)
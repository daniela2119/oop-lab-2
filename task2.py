class Enrollee():
    def __init__(self, surname, name, fathersName, adress, arrayOfMarks):
        self.surname = surname
        self.name = name
        self.fathersName = fathersName
        self.adress = adress
        self.mathMark = arrayOfMarks[0]
        self.physicsMark = arrayOfMarks[1]
        self.ukrLangMark = arrayOfMarks[2]

enrollee1 = Enrollee("Анастасьева", "Анастасия", "Игоревна", "Киев", [9, 10, 7])
enrollee2 = Enrollee("Горбатюк", "Сергей", "Михайлович", "Днепр", [11, 8, 7])
enrollee3 = Enrollee("Полякова", "Валерия", "Витальевна", "Белая Цевковь", [4, 5, 12])
enrollee4 = Enrollee("Гребеньков", "Антон", "Олегович", "Киев", [10, 9, 6])
enrollee5 = Enrollee("Лобанёв", "Денис", "Алексеевич", "Ивано-франковск", [11, 8, 12])

group = [enrollee1, enrollee2, enrollee3, enrollee4, enrollee5]

#код ниже выводит список абитуриентов, которые имеют неудовлетворительные оценки

for en in group:
    if 6 >= en.mathMark or 6 >= en.physicsMark or 6 >= en.ukrLangMark :
        print(en.surname + " " + en.name + " " + en.fathersName)

#код ниже выводит список абитуриентов, сумма баллов которых не меньше заданной

marks = input("Введите сумму баллов: ")

for en in group:
    if int(marks) <= en.mathMark + en.physicsMark + en.ukrLangMark:
        print(en.surname + " " + en.name + " " + en.fathersName)

#код ниже выводит n абитуриентов с наивысшей суммой баллов

n = input("Введите количество студентов: ")

group = [enrollee5, enrollee1, enrollee2, enrollee4, enrollee3]

for i in range(int(n)):
    print(group[i].surname + " " + group[i].name + " " + group[i].fathersName)
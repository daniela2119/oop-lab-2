class Scanner():
    def __init__(self, model, producer, price, width, height, opticalResolution, grayGradationsNumber):
        self.model = model
        self.producer = producer
        self.price = price
        self.width = width
        self.height = height
        self.opticalResolution = opticalResolution
        self.grayGradationsNumber = grayGradationsNumber

scanner1 = Scanner("Canon CanoScan", "Canon", 10000, 210, 297, "2400*2400", 48)
scanner2 = Scanner("CanoScan Lide 300", "Canon", 6500, 216, 297, "2400*4800", 48)
scanner3 = Scanner("Epson Perfection V19", "Epson", 21000,  210, 297, "4800*4800", 48)
scanner4 = Scanner("A4 Canon", "Canon", 88000, 250, 367, "2400*2400", 48)
scanner5 = Scanner("Epson Perfection V55", "Epson", 44000, 210, 297, "9600*6400", 48)

group = [scanner1, scanner2, scanner3, scanner4, scanner5]

#код ниже выводит информацию про сканер заданной модели производителя

model = input("Введите модель производителя: ")

for sc in group:
    if model == sc.model:
        print("Сканер", sc.model, "от", sc.producer)

#код ниже выводит список сканеров, которые поддерживают определённые размеры области сканирования

width = input("Введите ширину области сканирования: ")
height = input("Введите длину области сканирования: ")

for sc in group:
    if int(width) <= sc.width and int(height) <= sc.height:
        print("Сканер", sc.model, "от", sc.producer)

#код ниже выводит список сканеров с отличием в цене в пределах 5% от заданной

price = int(input("Введите площадь: "))
difference = (price / 100) * 5

for sc in group:
    if price - difference <= sc.price and price + difference >= sc.price:
        print("Сканер", sc.model, "от", sc.producer)
class Aeroflot():
    def __init__(self, destination, flightNumber, planeType, departureTime):
        self.destination = destination
        self.flightNumber = flightNumber
        self.planeType = planeType
        self.departureTime = departureTime

flight1 = Aeroflot("Киев", 191007, "Малый пассажирский", "12:34")
flight2 = Aeroflot("Днепр", 110807, "Средний пассажирский", "05:06")
flight3 = Aeroflot("Лондон", 240512, "Средний пассажирский", "08:07")
flight4 = Aeroflot("Киев", 100906, "Большой пассажирский", "09:01")
flight5 = Aeroflot("Ивано-франковск", 110812, "Большой пассажирский", "23:45")

group = [flight1, flight2, flight3, flight4, flight5]

#код ниже выводит ведомости про рейсы до заданного пункта назначения

destination = input("Введите пункт назначения: ")

for fl in group:
    if destination == fl.destination:
        print(fl.planeType + " самолёт рейса", fl.flightNumber, "отправляется в " + fl.destination + " в " + fl.departureTime)

#код ниже выводит список самолётов, которые летают рейсам в заданном направлении

destination = input("Введите пункт назначения: ")

print("В заданном направлении есть рейсы: ")
for fl in group:
    if destination == fl.destination:
        print(fl.flightNumber)

#код ниже выводит список рейсов к заданному пункту назначения, время вылета для которых больше заданного

time = input("Введите время вылета: ")
tHours = time[0:2]
tMinutes = time[3:]

for fl in group:
    hours = fl.departureTime[0:2]
    minutes = fl.departureTime[3:]
    if int(tHours) < int(hours):
        print(fl.flightNumber)
    elif int(tHours) == int(hours):
        if int(tMinutes) <= int(minutes):
            print(fl.flightNumber)
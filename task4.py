class Book():
    def __init__(self, author, title, publishingOffice, year, numberOfPages):
        self.author = author
        self.title = title
        self.publishingOffice = publishingOffice
        self.year = year
        self.numberOfPages = numberOfPages

book1 = Book("Джаннетт Уоллс", "Замок из стекла", "Бомбора", 2005, 330)
book2 = Book("Вероника Рот", "Дивергент", "Эксмо", 2011, 340)
book3 = Book("Харпер Ли", "Убить пресмешника", "АСТ", 1960, 350)
book4 = Book("Вероника Рот", "Инсургент", "Эксмо", 2012, 340)
book5 = Book("Александр Полярный", "Мятная сказка", "АСТ", 2018, 70)

group = [book1, book2, book3, book4, book5]

#код ниже выводит список книг заданного автора

author = input("Введите имя автора: ")

for bo in group:
    if author == bo.author:
        print(bo.title)

#код ниже выводит список книг, напечатанных занным издательством

publishingOffice = input("Введите издательство: ")

for bo in group:
    if publishingOffice == bo.publishingOffice:
        print(bo.title)

#код ниже выводит список книг, напечатанных позже заданного года

year = input("Введите издательство: ")

for bo in group:
    if int(year) <= bo.year:
        print(bo.title)
class Employee():
    def __init__(self, surname, initials, position, startYear, salary):
        self.surname = surname
        self.initials = initials
        self.position = position
        self.startYear = startYear
        self.salary = salary

employee1 = Employee("Анастасьева", "А. И.", "электрик", 2005, 1000)
employee2 = Employee("Горбатюк", "С. М.", "системный администратор", 2011, 6500)
employee3 = Employee("Полякова", "В. В.", "переводчик", 2010, 2100)
employee4 = Employee("Гребеньков", "А. О.", "разработчик", 2012, 8800)
employee5 = Employee("Лобанёв", "Д. А.", "системный администратор", 2018, 4400)

group = [employee1, employee2, employee3, employee4, employee5]

#код ниже выводит список работников, стаж которых превышает заданное число

experience = input("Введите стаж работника: ")

for em in group:
    if int(experience) <= 2020 - em.startYear:
        print(em.surname, em.initials)

#код ниже выводит список работников, зарплата которых больше заданной

salary = input("Введите зарплату: ")

for em in group:
    if int(salary) <= em.salary:
        print(em.surname, em.initials)

#код ниже выводит список работников, занимаемых заданную должность

position = input("Введите должность: ")

for em in group:
    if position == em.position:
        print(em.surname, em.initials)
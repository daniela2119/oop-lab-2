class Product():
    def __init__(self, name, producer, price, shelfLife, ammount):
        self.name = name
        self.producer = producer
        self.price = price
        self.shelfLife = shelfLife
        self.ammount = ammount

product1 = Product("чипсы", "Lays", 200, 36, 33)
product2 = Product("творог", "Галичина", 20, 1, 340)
product3 = Product("творог", "Рошен", 195, 3, 50)
product4 = Product("йогурт", "Галичина", 10, 1, 34)
product5 = Product("сосиски", "ФУД", 205, 1, 70)

group = [product1, product2, product3, product4, product5]

#код ниже выводит список товаров занного производителя

producer = input("Введите производителя: ")

for pr in group:
    if producer == pr.producer:
        print(pr.name)

#код ниже выводит список товаров для заданного наименования, цена которых не превышает заданной

name = input("Введите наименование товара: ")
price = input("Введите цену: ")

for pr in group:
    if name == pr.name and int(price) <= pr.price:
        print(pr.name + " от " + pr.producer)

#код ниже выводит список товаров, срок хранения которых больше заданного

shelfLife = input("Введите срок хранения: ")

for pr in group:
    if int(shelfLife) <= pr.shelfLife:
        print(pr.name)
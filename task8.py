class Pacient():
    def __init__(self, surname, name, fathersName, adress, hospitalCardNumber, diagnosis):
        self.surname = surname
        self.name = name
        self.fathersName = fathersName
        self.adress = adress
        self.hospitalCardNumber = hospitalCardNumber
        self.diagnosis = diagnosis

pacient1 = Pacient("Анастасьева", "Анастасия", "Игоревна", "Киев", 191007, "ОРЗ")
pacient2 = Pacient("Горбатюк", "Сергей", "Михайлович", "Днепр", 110807, "грипп")
pacient3 = Pacient("Полякова", "Валерия", "Витальевна", "Белая Цевковь", 240512, "грипп")
pacient4 = Pacient("Гребеньков", "Антон", "Олегович", "Киев", 100906, "перелом руки")
pacient5 = Pacient("Лобанёв", "Денис", "Алексеевич", "Ивано-франковск", 110812, "царапина на колене")

group = [pacient1, pacient2, pacient3, pacient4, pacient5]

#код ниже выводит список пациентов, имеющих опреденённый диагноз

diagnosis = input("Введите диагноз: ")

for pa in group:
    if diagnosis == pa.diagnosis:
        print(pa.surname, pa.name, pa.fathersName)

#код ниже выводит список пациентов, номер карты которых находится в заданном интервале

minNum = input("Введите минимальный номер карты: ")
maxNum = input("Введите максимальный номер карты: ")

for pa in group:
    if int(minNum) <= pa.hospitalCardNumber and int(maxNum) >= pa.hospitalCardNumber:
        print(pa.surname, pa.name, pa.fathersName)

#код ниже выводит список пациентов, фамилия которых начинается с заданной буквы

letter = input("Введите букву: ")

for pa in group:
    surnameLetter = pa.surname[0]
    if letter == surnameLetter:
        print(pa.surname, pa.name, pa.fathersName)
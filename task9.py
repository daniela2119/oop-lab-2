class Bus():
    def __init__(self, surname, initials, busNumber, wayNumber, model, runningYear, mileage):
        self.surname = surname
        self.initials = initials
        self.busNumber = busNumber
        self.wayNumber = wayNumber
        self.model = model
        self.runningYear = runningYear
        self.mileage = mileage

bus1 = Bus("Анастасьева", "А. И.", 191007, 330, "Малый пассажирский", 2005, 10000)
bus2 = Bus("Горбатюк", "С. М.", 110807, 340, "Средний пассажирский", 2011, 6500)
bus3 = Bus("Полякова", "В. В.", 240512, 350, "Средний пассажирский", 1960, 21000)
bus4 = Bus("Гребеньков", "А. О.", 100906, 341, "Большой пассажирский", 2012, 8800)
bus5 = Bus("Лобанёв", "Д. А.", 110812, 70, "Большой пассажирский", 2018, 44000)

group = [bus1, bus2, bus3, bus4, bus5]

#код ниже выводит список автобусов для заданного номера маршрута

wayNumber = input("Введите номер маршрута: ")

for bu in group:
    if int(wayNumber) == bu.wayNumber:
        print("Автобус №", bu.busNumber)

#код ниже выводит список автобусов, эксплуатирующихся больше 10 лет

for bu in group:
    if 10 <= 2020 - bu.runningYear:
        print("Автобус №", bu.busNumber)

#код ниже выводит список автобусов, пробег которых больше 10000км

for bu in group:
    if 10000 <= bu.mileage:
        print("Автобус №", bu.busNumber)